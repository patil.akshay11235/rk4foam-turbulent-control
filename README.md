# RK4Foam Turbulent Control

DNS solver in OpenFOAM using the RK4 time discretisation technique to obtain a turbulent transition within 7 eddy turn over times. The solver achieves statistical steady state for a specified target mean bulk velocity.

Note: To run the tutorial case, you need to install setDnsIc utility as well. This will set up the initial condition. No tests have been done when the U(t=0) = 0 within the domain. The claim of 7 eddy turn over time statistical steady state is based on the initial condition set using setDnsIc.
