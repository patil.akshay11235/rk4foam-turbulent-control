#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep  9 12:29:02 2020

@author: Akshay Patil

This file estimates the requisite parameters for the DNS case based on the Re_tau (friction Reynolds number)
"""
#%% IMPORTING THE LIBRARIES
import numpy as np

#%% SETTING UP THE PARAMETERS

alpha = 2.1                         #Grid expansion parameter
ny = 128                             #Number of grid points in the verical
scale = 0.1                           #Scaling factor for the geometry
nu = 1e-6                           #Kinematic viscosity
kappa = 0.4                        #von Karman constant
Href = 1                            #Reference channel height (Half height) if channel has top and bottom walls as no slip
Nx = 514                            #Number of points i streamwise direction
Nz = 258                            #number of points in the spanwise direction
Lx = 4*np.pi*scale
Lz = 2*np.pi*scale

dx = Lx/Nx
dz = Lz/Nz


Re_tau = 500                        #Target Retau
u_star = (Re_tau*nu)/(scale*Href)   #Target u_tau
print('Target u_star:',u_star)

#%% DATA PLACEHOLDERS
y = np.zeros(ny)            #Non-dimensional y
U = np.zeros(ny)            #Non-Dimensional velocity
Ud = np.zeros(ny)           #Dimensional velocity
Ulin = np.zeros(ny)         #Equivalent Linear velocity profile

#%% CREATING THE GRID
#Loop and prepare the grid
for i in range(ny):
    y[i] = (i-1)
    
y = (2*y/max(y)) - 1

#Tangent hyperbolic expansion
for i in range(ny):
    y[i] = np.tanh(alpha*y[i])/np.tanh(alpha)

#Rescale to normal coordinates
y = y - min(y)
y = 2*y/max(y)
y = scale*y                 #Final scaling of the geometry

#%% CREATING THE VELOCITY LOG-LAW

#Loop over the grid points to create the velocity profile
for ind in range(ny):
    if(y[ind]*(u_star/nu) <= 11):      #Viscous sub-layer
        U[ind] = y[ind]*(u_star/nu)
    else:
        U[ind] = (1/kappa)*np.log(y[ind]*(u_star/nu))+5.5   #Outer log layer


#Dimensionalise the velocity
Ud = U*u_star

#%% EQUIVALENT LINEAR PROFILE

#Compute the mean velocity of the profile
Usum = 0
ysum = 0
for i in range(ny):
    if (i == 0):
        Usum += Ud[i]*y[1]
        ysum += y[1]
    elif (i == ny):
        Usum += Ud[i]*y[-2]
        ysum += y[-2]
    else:
        Usum += Ud[i]*(y[i]-y[i-1])
        ysum += y[i]-y[i-1]
         
meanU = Usum/ysum             #Mean value of the velocity profile

print('Mean Velocity in the profile (Log): ',meanU)
print('Normal Mean (Log): ',np.mean(Ud))
Usum = 0

for i in range(ny):
    Ulin[i] = 2*meanU*(1 - abs(y[i]/Href - 1))

for i in range(ny):
    if (i == 0):
            Usum += Ud[i]*y[1]
    elif (i == ny):
            Usum += Ud[i]*y[-2]
    else:
            Usum += Ud[i]*(y[i]-y[i-1])

meanU = Usum/ysum

print('Mean Velocity in the profile (Linear): ',meanU)  
print('Normal Mean (Linear): ',np.mean(Ulin))

print('Impose this mean value:',2*meanU)    #This is the velocity to be imposed in setDnsIC

#%% Print the dy
dy = np.diff(y)

print("Maximum dx ~",dx*u_star/nu)
print("Maximum dy ~",max(dy)*u_star/nu,"Minimum dy ~",dy[0]*u_star/nu)
print("Maximum dz ~",dz*u_star/nu)

print("Impose pressure gradient",u_star**2/(Href*scale))


